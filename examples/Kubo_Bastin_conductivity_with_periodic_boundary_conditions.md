---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.9.1
  kernelspec:
    display_name: python3
    language: python
    name: python3
---

```python
import sys
sys.path.append('../')
```

```python
import numpy as np
import kwant

from bloch import wrap_velocity
from hamiltonians import haldane_pbc
```

```python
# plotting
import matplotlib.pyplot as plt
import holoviews as hv
hv.notebook_extension()
```

```python
def plot_dos_and_curves(dos, labels_to_data, ylabel="DoS [a.u.]"):
    plt.figure(figsize=(5,4))
    plt.fill_between(dos[0], dos[1], label="DoS [a.u.]",
                     alpha=0.5, color='gray')
    for label, (x, y) in labels_to_data:
        plt.plot(x, y, label=label, linewidth=2)
    plt.legend(framealpha=0.5)
    plt.xlabel("energy [t]")
    plt.ylabel(ylabel)
    
    plt.ylim(-2, 4)
```

```python
lx = 10
ly = 10
trans_symm = [(lx, 0), (0, ly)]
lat, syst = haldane_pbc(trans_symm, return_builder=True)
```

```python
fsyst = kwant.wraparound.wraparound(syst).finalized()

norbs = fsyst.sites[0].family.norbs
bounds = (-3.1, 3.1)
```

```python
velocity_builder = wrap_velocity(syst)
vx = velocity_builder.hamiltonian_submatrix(params=dict(k_x=0, k_y=0, direction=[1, 0]), sparse=True)
vy = velocity_builder.hamiltonian_submatrix(params=dict(k_x=0, k_y=0, direction=[0, 1]), sparse=True)
```

```python
params = dict(zip(fsyst._momentum_names, (0, 0)))
```

```python
area_per_site = np.abs(np.cross(*lat.prim_vecs)) / len(lat.sublattices)
```

```python
kwant.plot(fsyst);
```

```python
spectrum = kwant.kpm.SpectralDensity(fsyst, params=params)
```

```python
hv.Curve((spectrum.energies, spectrum.densities.real))
```

### using periodic velocity operators
(in a periodic system)

```python
s_factory = kwant.kpm.LocalVectors(fsyst, where=None)
vectors = list(s_factory)
num_vectors = len(vectors)

# number of sites per vector
norm_v = np.linalg.norm(vectors[0]) ** 2

cond_xx = kwant.kpm.conductivity(
    fsyst, params=params,
    bounds=bounds,
    alpha=vx,
    beta=vx,
    mean=False,
    num_vectors=num_vectors,
    vector_factory=vectors)

cond_xy = kwant.kpm.conductivity(
    fsyst, params=params,
    alpha=vx,
    beta=vy,
    mean=False,    
    num_vectors=num_vectors,
    vector_factory=vectors
)

energies = cond_xx.energies
cond_array_xx = np.array([cond_xx(e, temperature=0.0) for e in energies]).real
cond_array_xy = np.array([cond_xy(e, temperature=0.0) for e in energies]).real

# area of the unit cell per site
cond_array_xx /= area_per_site
cond_array_xy /= area_per_site

cond_array_xx = cond_array_xx / norm_v
cond_array_xy = cond_array_xy / norm_v
```

```python
plot_dos_and_curves(
    (spectrum.energies, spectrum.densities.real / (lx * ly / 3)),
    [
        (r'Longitudinal conductivity $\sigma_{xx}/L_x$',
         (spectrum.energies, cond_array_xx.mean(1) / (lx))),
        (r'Hall conductivity $\sigma_{xy}$',
         (spectrum.energies, cond_array_xy.mean(1)))],
    ylabel=r'$\sigma [e^2/h]$'
);
```

### using open boundary velocity operators
(in a periodic system..! don't expect correct results)

```python
s_factory = kwant.kpm.LocalVectors(fsyst, where=None)
vectors = list(s_factory)
num_vectors = len(vectors)

# number of sites per vector
norm_v = np.linalg.norm(vectors[0]) ** 2

cond_xx = kwant.kpm.conductivity(
    fsyst, params=params,
    bounds=bounds,
    alpha='x',
    beta='x',
    mean=False,
    num_vectors=num_vectors,
    vector_factory=vectors)

cond_xy = kwant.kpm.conductivity(
    fsyst, params=params,
    alpha='x',
    beta='y',
    mean=False,    
    num_vectors=num_vectors,
    vector_factory=vectors
)

energies = cond_xx.energies
cond_array_xx = np.array([cond_xx(e, temperature=0.0) for e in energies]).real
cond_array_xy = np.array([cond_xy(e, temperature=0.0) for e in energies]).real

# area of the unit cell per site
cond_array_xx /= area_per_site
cond_array_xy /= area_per_site

cond_array_xx = cond_array_xx / norm_v
cond_array_xy = cond_array_xy / norm_v
```

```python
plot_dos_and_curves(
    (spectrum.energies, spectrum.densities.real / (lx * ly / 3)),
    [
        (r'Longitudinal conductivity $\sigma_{xx} / L_x$',
         (spectrum.energies, cond_array_xx.mean(1) / (lx))),
        (r'Hall conductivity $\sigma_{xy}$',
         (spectrum.energies, cond_array_xy.mean(1)))],
    ylabel=r'$\sigma [e^2/h]$'
);
```

```python

```
