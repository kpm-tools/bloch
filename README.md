Module to handle operators applied to systems with periodic boundary conditions.

The systems should be defined as Kwant Builders with translational symmetries.

The version of Kwant needs to be greater than "1.4.2" or the development versions.

Check the Kwant instructions on how build Kwant from source:

https://kwant-project.org/doc/1/pre/install
